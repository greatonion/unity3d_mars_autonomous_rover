# Autonomous Mars Rover

![](https://gitlab.com/greatonion/unity3d_mars_autonomous_rover/raw/master/docs/start.png)

This project is modeled after the [NASA sample return challenge](https://www.nasa.gov/directorates/spacetech/centennial_challenges/sample_return_robot/index.html) and it gives some experience with the three essential elements of robotics, which are perception, decision making and actuation.  It based on Vison Systems / OpenCV and simulator environment built with the Unity3D game engine. 

###### Thank you Unity for free and easy to use game engine!

## The Simulator
 [Windows](https://gitlab.com/greatonion/unity3d_mars_autonomous_rover/blob/master/simulator/Windows_Roversim.zip) 
Simulator has two modes. Manual - to test how to drive with rover on mars. And autonomus that allows to controll rover via python scripts.


## Dependencies (python env)
```
asn1crypto                0.24.0                   py35_0    conda-forge
backports                 1.0                      py35_1    conda-forge
backports.functools_lru_cache 1.5                      py35_0    conda-forge
backports.weakref         1.0rc1                    <pip>
bleach                    2.1.3                      py_0    conda-forge
bleach                    1.5.0                     <pip>
ca-certificates           2018.1.18                     0    conda-forge
certifi                   2018.1.18                py35_0    conda-forge
cffi                      1.11.5                   py35_0    conda-forge
chardet                   3.0.4                     <pip>
click                     6.7                        py_1    conda-forge
colorama                  0.3.9                    py35_0    conda-forge
cryptography              2.1.4                    py35_0    conda-forge
cycler                    0.10.0                   py35_0    conda-forge
dask-core                 0.17.2                     py_0    conda-forge
decorator                 4.0.11                    <pip>
decorator                 4.2.1                    py35_0    conda-forge
entrypoints               0.2.3                    py35_1    conda-forge
eventlet                  0.22.0                     py_0    conda-forge
fastcache                 1.0.2                    py35_0    conda-forge
ffmpeg                    2.7.0                         0    menpo
flask                     0.12.2                   py35_0    conda-forge
flask-socketio            2.9.3                      py_0    conda-forge
freetype                  2.8.1                    vc14_0  [vc14]  conda-forge
greenlet                  0.4.13                   py35_0    conda-forge
h5py                      2.7.1                    py35_3    conda-forge
hdf5                      1.10.1                   vc14_2  [vc14]  conda-forge
html5lib                  0.9999999                 <pip>
html5lib                  1.0.1                      py_0    conda-forge
icu                       58.2                     vc14_0  [vc14]  conda-forge
idna                      2.6                      py35_1    conda-forge
imageio                   2.1.2                    py35_0    conda-forge
ipykernel                 4.8.2                    py35_0    conda-forge
ipython                   6.2.1                    py35_1    conda-forge
ipython_genutils          0.2.0                    py35_0    conda-forge
ipywidgets                5.1.5                    py35_0    menpo
itsdangerous              0.24                       py_2    conda-forge
jedi                      0.11.1                   py35_0    conda-forge
jinja2                    2.10                     py35_0    conda-forge
jpeg                      9b                       vc14_2  [vc14]  conda-forge
jsonschema                2.6.0                    py35_1    conda-forge
jupyter                   1.0.0                    py35_0    conda-forge
jupyter_client            5.2.3                    py35_0    conda-forge
jupyter_console           5.2.0                    py35_0    conda-forge
jupyter_core              4.4.0                      py_0    conda-forge
kiwisolver                1.0.1                    py35_1    conda-forge
libpng                    1.6.34                   vc14_0  [vc14]  conda-forge
libtiff                   4.0.9                    vc14_0  [vc14]  conda-forge
Markdown                  2.6.11                    <pip>
markupsafe                1.0                      py35_0    conda-forge
matplotlib                2.2.2                    py35_0    conda-forge
mistune                   0.8.3                      py_0    conda-forge
mkl                       2017.0.3                      0
moviepy                   0.2.3.2                   <pip>
mpmath                    1.0.0                      py_0    conda-forge
nb_conda                  2.2.1                    py35_0    conda-forge
nb_conda_kernels          2.1.0                    py35_0    conda-forge
nbconvert                 5.3.1                      py_1    conda-forge
nbformat                  4.4.0                    py35_0    conda-forge
networkx                  2.1                      py35_0    conda-forge
notebook                  5.4.1                    py35_0    conda-forge
numpy                     1.13.1                   py35_0
olefile                   0.45.1                   py35_0    conda-forge
opencv3                   3.1.0                    py35_0    menpo
openssl                   1.0.2n                   vc14_0  [vc14]  conda-forge
pandas                    0.22.0                   py35_0    conda-forge
pandoc                    2.1.3                         0    conda-forge
pandocfilters             1.4.1                    py35_0    conda-forge
parso                     0.1.1                      py_0    conda-forge
patsy                     0.5.0                    py35_0    conda-forge
pickleshare               0.7.4                    py35_0    conda-forge
pillow                    5.0.0                    py35_0    conda-forge
pip                       9.0.1                    py35_1    conda-forge
prompt_toolkit            1.0.15                   py35_0    conda-forge
protobuf                  3.5.2.post1               <pip>
pycparser                 2.18                     py35_0    conda-forge
pygments                  2.2.0                    py35_0    conda-forge
pyopenssl                 17.5.0                   py35_0    conda-forge
pyparsing                 2.2.0                    py35_0    conda-forge
pyqt                      5.6.0                    py35_4    conda-forge
PyQt5                     5.10.1                    <pip>
pyqtgraph                 0.10.0                    <pip>
pyreadline                2.1                      py35_0    conda-forge
python                    3.5.4                         0
python-dateutil           2.7.0                      py_0    conda-forge
python-engineio           2.0.2                      py_0    conda-forge
python-socketio           1.8.4                      py_0    conda-forge
pytz                      2018.3                     py_0    conda-forge
pywavelets                0.5.2                    py35_1    conda-forge
pywinpty                  0.5                      py35_1    conda-forge
pyzmq                     17.0.0                   py35_3    conda-forge
qt                        5.6.2                    vc14_1  [vc14]  conda-forge
qtconsole                 4.3.1                    py35_0    conda-forge
requests                  2.18.4                    <pip>
scikit-image              0.13.1                   py35_0    conda-forge
scikit-learn              0.19.0              np113py35_0
scipy                     0.19.1              np113py35_0
seaborn                   0.8.1                    py35_0    conda-forge
send2trash                1.5.0                      py_0    conda-forge
setuptools                39.0.1                   py35_0    conda-forge
simplegeneric             0.8.1                    py35_0    conda-forge
sip                       4.18                     py35_1    conda-forge
sip                       4.19.8                    <pip>
six                       1.11.0                   py35_1    conda-forge
socketIO-client           0.7.2                     <pip>
statsmodels               0.8.0                    py35_0    conda-forge
sympy                     1.1.1                    py35_0    conda-forge
tensorflow                1.2.1                     <pip>
terminado                 0.8.1                    py35_0    conda-forge
testpath                  0.3.1                    py35_0    conda-forge
toolz                     0.9.0                      py_0    conda-forge
tornado                   5.0.1                    py35_1    conda-forge
tqdm                      4.11.2                    <pip>
traitlets                 4.3.2                    py35_0    conda-forge
transforms3d              0.3.1                     <pip>
urllib3                   1.22                      <pip>
vc                        14                            0    conda-forge
vs2015_runtime            14.0.25420                    0    conda-forge
wcwidth                   0.1.7                    py35_0    conda-forge
webencodings              0.5                      py35_0    conda-forge
websocket-client          0.47.0                    <pip>
werkzeug                  0.14.1                     py_0    conda-forge
wheel                     0.30.0                   py35_2    conda-forge
widgetsnbextension        1.2.3                    py35_1    menpo
win_unicode_console       0.5                      py35_0    conda-forge
wincertstore              0.2                      py35_0    conda-forge
winpty                    0.4.3                    vc14_2  [vc14]  conda-forge
zlib                      1.2.11                   vc14_0  [vc14]  conda-forge

```

## How to map area (Notebook Analysis)

This chapter supose to explain how Mars map was done. 
All code was developed in jupyter notebook that can be found [here](map_area.ipynb)


In provided simulator there is quite easy way how to distinguish between what is road and what road is not. 
Basically road is bright and mountains are dark. So to find navigable area is as simple as able to select between dark and bright

### how to find road

In order to find road what is need is proper color thresholding. In my case road was perefctly everything where pixel is 
brighter than 160 (in 8 bit world) in all three RGB channels.

 ```python
rgb_thresh=(160, 160, 160)
above_thresh = (img[:,:,0] > rgb_thresh[0]) \
                & (img[:,:,1] > rgb_thresh[1]) \
                & (img[:,:,2] > rgb_thresh[2])
```
Navigable area is find by function 
`color_thresh()`
### how to find obstacle


As said before obstacle is everything what is not road. So inverting the logic is quite accurate

`obstacles = color_thresh(~warped & mask)`

### How to find rocks

Finding rocks is a litlle more tricky as 

### accuracy boost
As these methods where good enough to map robot area on the video you can find in the end of 

### Masking
As image after perspective transform may be very distorted (mostly on the edges) i managed to cut off some of the 
perypherial pixels

```python
mask = perspect_transform(np.ones_like(grid_img)*255, source, destination)
mask[0:60,:] = 0
mask[:,0:50] = 0
mask[:,270:] = 0


rock_mask = np.copy(mask)
rock_mask[0:110,:] = 0
```

for finding rocks mask is even more agresive because rocks are quite high, small mask can prevent a little from disortion
on the picture below mask is and result of perspective warp is presented for sample images.
In first row navigable terrain detection, than obstacle than rocks

![](docs/thresholds.png)


### Recording Data
Data set is saved in `test_dataset` directory.  In that folder is also a csv file with the output data for steering, throttle position etc. and the pathnames to the images recorded in each run.  There are also few images in the folder called `calibration_images` to do some of the initial calibration steps with.  

The first step of this project is to record data.  It's quite easy done with simulator.Launch the simulator and choose "Training Mode" then hit "r".  Navigate to the directory you want to store data in, select it, and then drive around collecting data.  Hit "r" again to stop data collection.

### Data Analysis
Included in the IPython notebook called `map_area.ipynb` are the functions for performing the various steps of this project.  The notebook should function as is without need for modification at this point. 

### Image processing pipe
Core of the `map_area.ipynb` file is process_image() function. It allows to build 


First image is transfered to the warping function. As robot see world in very difrent perspective than maps are designed. 
However thanks to the `perspect_transform()` it's 

Basicaly this function is processed in every position of robot. 
Knowing robot localization and be able to change perspective from cam view to map view allows as build a map


```python
Take image and robot real world yaw and position
\/
Warp it to map perspective
\/
Create pixelmaps for navigable area, obstacles, and rocks
\/
Apply map perspective to real position and yaw
\/
Enjoy map building

```

### Map reconstruction
Map reconstruction is done by singleton `data` storing data from each time function was processed.

```python
    data.worldmap[road_world_y,road_world_x,2] = 255
    data.worldmap[obs_world_y,obs_world_x,0] = 255
    data.worldmap[rock_world_y, rock_world_x,1] = 255
```

At the very evry frame caught in training process is analyzed and transfered to the worldmapobject.
The result of the mapping is available at the end of the jupyter file, or on 
[here](output/test_mapping.mp4) 


#### Small map reconstruction trick

As it is very possible that obstacle will collide with navigable area there is a function 
that clears all places firstly found as obstacle but now finds as road.

```python
    #clear road
    is_road = data.worldmap[:,:,2] > 0
    data.worldmap[is_road,0] = 0
```


## Navigating Autonomously

### [Here](https://youtu.be/2DgtIrPDRA4) is the video from the autonomous ride. 
![](docs/ytb.png)

Project criteria were met:
###### in 5 minutes ride:
- over 82% of the map was covered
- data accuracy was above 88%
- 6/6 samples were found


###### in 15 minutes ride:
- over 95% of the map was covered
- data accuracy was above 75%
- 6/6 samples were found



#### Perception

In `PerceptionStep()` used in `code/drive_rover.py` map wasp built exaxtly the same way as tested in jupyter notebook in previous paragraph. Including one simple change that this time additionaly estimated angle of the road is calculated with the `to_polar_coords()` method.


#### Decisions 

In order to force robot to move simple decision machine state was implemented. In `decision_step()` function all control is based on the calculated angle from previous paragraph. Simply what is done is 
1) Take the actual 'mean angle' of the road 
2) move to that direction 
3) If angle is to small (robot is nerby obstacle) STOP
4) IF Stop rotate until 'mean angle of the road' is high enough. And move to this direction


#### High accuracy trick
I'm quite happy with the accuracy result ( 88% vs 60% required). It was achieved thanks to agresive and asymetric(3,5) erode of the road detection signal and dilatation of the obstacles signal. 

```
    kernel = np.ones((3,5),np.uint8)
    road = cv2.erode(road,kernel,iterations = 3)

    kernel = np.ones((5,5),np.uint8)
    obstacles = cv2.dilate(obstacles,kernel,iterations = 2)
```


## Summary and possible improvments

- It's posible to map area from robot based only on camera view and GPS signals
- Roads and obstacles could be detected with much more accurate algorithms based on deep learning 
- Obstacles could be detected in 3D space using LIDARs etc.
- Mapping could be improved by adding rotation of camera in horizontal axis


